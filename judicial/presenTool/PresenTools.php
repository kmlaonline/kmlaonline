<?php
require_once(dirname(__FILE__)."/PresenArticle.php");
require_once(dirname(__FILE__)."/PresenMember.php");
require_once(dirname(__FILE__)."/PresenAccuser.php");
require_once(dirname(__FILE__)."/PresenArticleKind.php");
function initializePresenTools($server, $id, $pw, $dbname, $force_renew=false){

    if($force_renew){
        $db=new mysqli($server, $id, $pw);
        $db->query("drop database " . $dbname);
        $db->close();
    }

    global $article_kind, $member, $db, $article, $accuser;
    $db=@new mysqli($server, $id, $pw, $dbname);
    /*$newdb=false;
    if($db->connect_error){
        $newdb=true;
        $db=new mysqli($server, $id, $pw);
        if($db->connect_error){
            return false;
        }
        $db->query("create database " . $dbname);
        $db->query("use " . $dbname);
        $db->set_charset("utf8");
    }
    $member=new Soreemember($db, "{$dbname}_member");
    $board=new Soreeboard($db,"{$dbname}_board", $member);
    if($newdb || $force_renew){
        $member->prepareFirstUse();
        $board->prepareFirstUse();
    } */
    if($db->connect_error){
        echo "<b>Error occured when connecting db</b>";
        return false;
    }
    $article = new PresenArticle($db);
    $member = new PresenMember($db);
    $accuser = new PresenAccuser($db);
    $article_kind = new PresenArticleKind($db);
    return $db;
};
?>