<!DOCTYPE html>
<html ng-app="teacherApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <link rel="icon" href="../../favicon.ico">
        <title>KMLA Online - 교직원 큼온</title>
        <!-- Bootstrap core CSS -->
        <link href="/teacher/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="/teacher/css/dashboard.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="/teacher/js/angular.js"></script>
        <script type="text/javascript" src="/teacher/js/main.js"></script>
    </head>
    <body ng-controller="teacherCtrl">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/teacher/main">교직원 큼온</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <!--li><a href="#">Dashboard</a></li-->
                        <li><a href="#">설정</a></li>
                        <li><a href="#">내 정보</a></li>
                        <li><a href="/user/logout">로그아웃</a></li>
                    </ul>
                    <!--form class="navbar-form navbar-right">
                        <input type="text" class="form-control" placeholder="Search...">
                        </form-->
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li><a ng-click="changePage('main')">메인</a></li>
                        <li><a ng-click="changePage('bbq')">바베큐 신청 현황</a></li>
                        <li><a href="#">공지 사항</a></li>
                        <li><a href="#">학생 건의 사항</a></li>
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <ng-include src="page"></ng-include>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="/teacher/js/bootstrap.js"></script>
    </body>
</html>