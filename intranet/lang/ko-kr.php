<?php
$_LANGUAGE=array(

    "board"=>array(
        "category"=>array(
            "no manage"=>"카테고리 관리 권한이 없습니다.",
            "nonexist"=>"카테고리가 존재하지 않습니다.",
            "added"=>"카테고리가 추가되었습니다.",
            "dupe"=>"카테고리 이름이 중복되거나 비어 있습니다.",
            "child exists"=>"카테고리에 글이 있습니다.",
            "request clear"=>"먼저 카테고리를 비워 주세요."
        ),
        "article"=>array(
            "unspecified"=>"글이 지정되지 않았습니다.",
            "data required"=>"내용을 입력하셔야 합니다.",
            "write unpermitted"=>"글을 쓸 수 없는 게시판입니다.",
            "title required"=>"제목을 입력하셔야 합니다.",
            "nonexist"=>"글이 존재하지 않거나 삭제되었습니다.",
            "notmine"=>"내 글이 아닙니다.",
            "parent removed"=>"댓글을 달 게시물이 제거되었습니다.",
            "child unpermitted"=>"댓글을 달 수 없는 게시물입니다.",
            "write comment"=>"댓글 쓰기",
            "writing comment"=>"덧글 등록 중..."
        )
    ),
    "file"=>array(
        "process error"=>"파일 처리 중에 오류가 발생하였습니다.",
    ),
    "message"=>array(
        "tagged"=>"%1%님이 글 <b>%2%에 태그했습니다."
    ),
    "term"=>array(
        "anonymous"=>"익명"
    ),
    "user"=>array(
        "permission"=>array(
            "error"=>array(
                "admin"=>"관리자 권한이 없습니다.",
                "login"=>"로그인해야 합니다.",
                "lack"=>"접근이 거부되었습니다."
            )
        ),
        "message"=>array(
            "id unspecified"=>"쪽지 ID가 지정되지 않았습니다.",
            "not mine"=>"내 쪽지가 아닙니다.",
        ),
        "register"=>array(
            "title"=>"회원가입",
            "tos"=>"... 아직 안 만들었어요.",
            "accept tos"=>"이용 약관에 동의합니다.",
            "required"=>"필수 정보",
            "optional"=>"선택적 정보",
            "password check"=>"비밀번호 확인",
            "captcha"=>"사람 확인",
            "ok"=>"가입"
        ),
        "settings"=>array(
            "password prev"=>"이전 패스워드",
            "save"=>"저장",
            "myinfo"=>array(
                ""=>"내 정보"
            ),
            "mainboard"=>array(
                ""=>"첫 화면 관리"
            ),
            "menu"=>array(
                ""=>"메뉴 관리"
            ),
            "mypage"=>array(
                ""=>"내 페이지"
            ),
            "message"=>array(
                ""=>"쪽지"
            )
        )
    ),
    "generic"=>array(
        "unknown error"=>"무언가가 잘못되었습니다.",
        "try later"=>"나중에 다시 시도해 주세요.",
        "tried"=>"작업을 시도하였습니다.",
        "succeed"=>"작업을 성공하였습니다.",
        "failed"=>"작업을 실패하였습니다.",
        "loading"=>"불러오는 중...",
        "tos"=>"이용 약관",
        "login"=>"로그인",
        "add"=>"추가",
        "setting"=>"설정",
        "manage"=>"관리",
        "information"=>"정보",
        "message"=>"쪽지",
        "admin"=>"관리자",
        "logout"=>"로그아웃",
        "edit"=>"수정",
        "revert"=>"되돌리기",
        "remove"=>"삭제",
        "save"=>"저장",
        "reply"=>"답장",
        "left"=>"남음",
        "list"=>"목록",
        "index"=>"번호",
        "id"=>"ID",
        "password"=>"비밀번호",
        "level"=>"기수",
        "name"=>"이름",
        "nick"=>"영어 이름",
        "choose file"=>"파일 선택",
        "birthday"=>"생일",
        "year"=>"년",
        "month"=>"월",
        "day"=>"일",
        "findid"=>"ID 찾기",
        "findpw"=>"비밀번호 찾기",
        "picture"=>"사진",
        "icon"=>"아이콘",
        "status message"=>"상태 메시지",
        "homepage"=>"홈페이지",
        "phone"=>"전화번호",
        "interest"=>"관심사",
        "suggest"=>"선택 사항도 최대한 자세히 입력해주세요.",
        "gender"=>"성별",
        "unspecified"=>"지정하지 않음",
        "male"=>"남자",
        "female"=>"여자",
        "other"=>"기타",
        "email"=>"E-Mail",
        "student_id"=>"학번",
        "find"=>"찾기",
        "findnext"=>"계속 찾기",
        "misc"=>"기타",
        "insert short"=>"넣기",
        "description short"=>"설명",
        "remove short"=>"삭제",
        "full date format"=>"Y-m-d H:i:s",
    ),
    "error pages"=>array(
        "403"=>array(
            "title"=>"접근 권한 없음",
            "big"=>"접근 권한이 없습니다.",
            "detail"=>"링크를 잘못 입력하셨는지 확인하고 클릭해서 들어왔다면 보고해 주세요."
        ),
        "404"=>array(
            "title"=>"잘못된 페이지",
            "big"=>"잘못된 페이지입니다.",
            "detail"=>"링크를 잘못 입력하셨는지 확인하고 클릭해서 들어왔다면 보고해 주세요."
        )
    ),
    "link titles"=>array(
        "board"=>array(
            "main"=>"게시판 %1%",
            "write"=>"글쓰기",
            "edit"=>"편집하기",
            "comment write"=>"댓글 쓰기",
            "delete confirm"=>"삭제 확인",
        ),
        "util"=>array(
            "important"=>"필수공지 설정",
            "lectureroom"=>"공동강의실 신청"
        ),
        "user"=>array(
            "settings"=>"사용자 설정"
        ),
        ""=>"처음으로"
    ),
    "admin"=>array(
        "only"=>"관리자 전용",
        "override"=>"모든 권한 갖기",
        "manage"=>array(
            "user"=>"사용자 관리",
            "category"=>"분류 관리",
            "misc"=>"기타 권한 관리"
        ),
        "user"=>array(
            "select first"=>"먼저 유저를 선택해 주세요."
        )
    ),
    "layout"=>array(
        "menu"=>array(
            "personalization"=>array(
                ""=>"개인화",
                "settings"=>"메뉴 설정",
                "remove"=>"숨기기"
            )
        ),
        "footer"=>'<div style="display: table;/* width: 100%; */margin: auto;width: calc(58% + 320px);"><div style="display: table-cell;text-align: left;">닷넷 2007 - '.date("Y").'</div><div style="display: table-cell;text-align: right;"><img src="/images/kmlaonline.png" style="/* display: block; *//* margin: auto; */height: 24px;display: inline-block;/* float: right; */"></div></div>'
    )
);

function lang($key1,$key2,$key3=null,$key4=null){
    global $_LANGUAGE;
    if(is_null($key3)) return htmlspecialchars(isset($_LANGUAGE[$key1][$key2])?$_LANGUAGE[$key1][$key2]:"$key1 $key2");
    if(is_null($key4)) return htmlspecialchars(isset($_LANGUAGE[$key1][$key2][$key3])?$_LANGUAGE[$key1][$key2][$key3]:"$key1 $key2 $key3");
    return htmlspecialchars(isset($_LANGUAGE[$key1][$key2][$key3][$key4])?$_LANGUAGE[$key1][$key2][$key3][$key4]:"$key1 $key2 $key3 $key4");
}